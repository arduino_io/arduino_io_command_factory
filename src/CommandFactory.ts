function describe(): string {
	return "d"
}

function read(name: string): string {
	return `r,${name}`
}

function write(name: string, data: boolean | number): string {
	if (typeof data === "boolean") {
		return `w,${name},${data ? "true" : "false"}`
	} else {
		return `w,${name},${data}`
	}
}

const CommandFactory = {
	read,
	write,
	describe,
}

export { CommandFactory }
